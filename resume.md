#蔡国
杭州-西湖区三墩镇  
18658865208  
1984-12-19  
[caiguo@cai-guo.com](mailto:caiguo@cai-guo.com)  
[gthub账户](https://github.com/caiguo37)  


个人简介
--------

1. 2011年开始开始从事iOS相关工作，精通Objective-C，熟悉Swift，精通OS X/iOS下的并行开发、网络、内存管理、GUI开发，熟悉各种常见网络框架(AFNetworking，ASIHTTPRequest，RestKit)，各种常见模型层框架(Core Data，Migical Record，Realm，Mantle，YYModel)。
2. 有多款上Store产品，独立开发、带领团队开发过多款产品。
3. 熟悉iOS开发从项目启动到上线审核发布的各个环节，熟悉Fastlane，Jenkins等自动化工具。曾在多家公司负责搭建iOS整体流程，通过各种自动化工具规范化、简化开发流程。
4. 5年Python， 4年Ruby经验，并有多种其他编程语言比如Clojure，Go的开发经验。有快速搭建服务器配合iOS开发，使用脚本自动化编译、打包、回归测试的能力。  


工作经历
--------

1. 2016年6月至今:

    公司：杭州闪电购

    岗位：技术专家 

    岗位描述：作为iOS团队负责人，负责闪电购整体的iOS业务及团队建设。标准化、自动化iOS应用开发流程。

    - 所有APP均采用了模块化设计，每个APP内部按业务划分为多个模块，各模块可独立发布，多个模块在多个APP中重用。
    - Objective-C与Swift混编
    - 部分APP部分页面由React Native实现
    - 基于Restful API与后端通讯
    - 基于Fastlane、CocoaPods，模块发布、发布TestFlight、上线审核全部都实现了自动化

    

2. 2015年1月至2016年6月:

    公司：阿⾥巴巴

    岗位：资深⽆线开发⼯程师 

    岗位描述：主要负责支付宝iOS客户端卡包业务。在团队内部推进函数式编程，并多次分享技术⽂章、在团队内部进行技术分享，协助提升团队内部代码质量。带领虚拟技术团队，持续关注业界技术发展，引进并修改ComponentKit、React Native等先进技术，并在具体项⽬目中落地。 

3. 2013年10月至2015年1月 ：  
    公司：[杭州弦音信息技术有限公司](http://eumlab.com/)  
    岗位：iOS开发工程师  
    岗位描述：负责多款iOS客户端的开发，负责多款产品需要的基础库的迁移、重构，维护公司内部基本库，维护公司私有CocoaPods库。在公司内推进项目结构改进，推进流程优化，推进UI Automation，CocoaPods等优秀工具的使用，并取得良好的效果。  

4. 2011年5月至2013年10月：  
    公司：[杭州点云信息技术有限公司](http://ask360.me/)  
    岗位：iOS开发工程师  
    岗位描述：带领团队负责Keep的开发。

    - Keep是一款类似微信的IM聊天
    - 聊天项目的数据基于CoreData存储
    - 通过Restful Api和XMPP与服务端进行通讯
    - 对于XMPP在一些运营商可能掉线问题做了些优化

5. 2010年10月至2011年5月：  

    公司：[上海阿耳法信息技术有限公司](http://www.alpha-cn.com)  

    岗位：Python开发工程师，iPhone客户端开发工程师  

    岗位描述：一个基于Django的web服务器端开发，全媒体采编系统的iOS客户端开发。  


## 个人负责的APP

1. [联华鲸选](https://itunes.apple.com/cn/app/id1240897255?mt=8)
2. [闪电购便利店](https://itunes.apple.com/cn/app/id918137885?mt=8)

## 个人参与的APP

1. [http://eumlab.com/pro-metronome/](http://eumlab.com/pro-metronome/)
2. [http://eumlab.com/iuke/](http://eumlab.com/iuke/)    
3. [http://eumlab.com/uketube/](http://eumlab.com/uketube/)  
4. [http://eumlab.com/drum-loops/](http://eumlab.com/drum-loops/) 


教育经历
--------

1. 2002年9月--2006年6月  
    中国计量学院	本科  
    光信息科学与技术  


语言能力
--------

英语（良好）：听说（良好），读写（精通）  
英语等级：英语四级  
